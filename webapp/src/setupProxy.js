const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/api/1',
    createProxyMiddleware({
      target: 'http://eurocamp-api:8080',
      changeOrigin: true,
    })
  );
};