import React, { useState, SyntheticEvent } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import './App.css';

import UsersPage from './pages/Users';
import ParcsPage from './pages/Parcs';
import BookingsPage from './pages/Bookings';
import Loading from './components/Loading';
import MessageBox from './components/MessageBox';

function App() {
  const [value, setValue] = useState(0);
  const handleChange = (event: SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return (
    <div className="App">
      <MessageBox />
      <Loading />
      <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
        <Tab label="Users" />
        <Tab label="Parcs" />
        <Tab label="Bookings" />
      </Tabs>
      {value === 0 && <UsersPage />}
      {value === 1 && <ParcsPage />}
      {value === 2 && <BookingsPage />}
    </div>
  );
}

export default App;
