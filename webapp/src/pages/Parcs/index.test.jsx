import ParcsPage from ".";
import { render, screen, waitFor, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../../store';

it('Test Click Get All Parcs', async () => {
    global.fetch = jest.fn().mockResolvedValue({ 
        status: 200,
        json: () => (
            {
                "data": [
                    {
                        "id": "34385",
                        "name": "facere",
                        "description": "Aliquid exercitationem quibusdam labore expedita."
                    },
                    {
                        "id": "1896",
                        "name": "alias",
                        "description": "Magnam quis dolore doloremque."
                    },
                    {
                        "id": "97671",
                        "name": "sit",
                        "description": "Ea voluptatum dolorum quas."
                    }
                ]
            }
        )
    });

    const {getByTestId, asFragment} = await render(<Provider store={store}><ParcsPage /></Provider>);

    await act(() => {
        const button = getByTestId('btn-fetch-all-parcs');
        userEvent.click(button);
    });

    const el = await screen.getByTestId('tf')
    expect(el).toHaveTextContent(/facere,Aliquid exercitationem quibusdam labore expedita/i)
});