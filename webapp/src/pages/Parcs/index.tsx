import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { Parc } from '../../reducers/parcs';
import { fetchAllParcs, fetchParcsById, deleteParcs, createParcs } from '../../api';
import { AppDispatch } from '../../store';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function ParcsPage() {
    const dispatch = useDispatch<AppDispatch>();
    const parcs:Array<Parc> = useSelector( (state:RootState) => state.parcs);

    let value = '';

    let id:string = parcs.length > 0 ? parcs[0].id : '';

    parcs.forEach((u:Parc)=>{
        if (value !== '') {
            value += "\n";
        }
        value += `${u.id},${u.name},${u.description}`;
    });

    function onClickCreateParcs() {
        const t = Date.now();
        let name = `ParcName${t}`;
        let description = `ParcDescription${t}`;
        dispatch(createParcs({name, description}));
    }
    
    return (
        <div>
            <Button data-testid="btn-fetch-all-parcs" variant="contained" onClick={()=>{dispatch(fetchAllParcs());}}>Fetch All Parcs</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(fetchParcsById({id}));}}>Fetch Parcs With ID (Id)</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(fetchParcsById({id:'parcId'}));}}>Fetch Parcs With ID Error</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(deleteParcs({id}));}}>Delete Parcs With ID (Id)</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(deleteParcs({id:'parcId'}));}}>Delete Parcs With ID Error</Button>
            <br></br>
            <Button variant="contained" onClick={onClickCreateParcs}>Create New Parcs Success / Error</Button>
            <br></br>
            <TextField inputProps={{'data-testid':"tf"}}  fullWidth multiline value={value}/>
        </div>
    );
}

export default ParcsPage;