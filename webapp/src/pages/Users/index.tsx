import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { User } from '../../reducers/users';
import { fetchAllUsers, fetchUsersById, deleteUsers, createUser } from '../../api';
import { AppDispatch } from '../../store';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function UsersPage() {
    const dispatch = useDispatch<AppDispatch>();
    const users:Array<User> = useSelector( (state:RootState) => state.users);

    let value = '';

    let userId:string = users.length > 0 ? users[0].id : '';

    users.forEach((u:User)=>{
        if (value !== '') {
            value += "\n";
        }
        value += `${u.id},${u.name},${u.email}`;
    });

    function onClickCreateUser() {
        let name = `User${Date.now()}`;
        let email = `${name}test.com`;
        dispatch(createUser({name, email}));
    }
    
    return (
        <div>
            <Button data-testid="btn-fetch-all-users" variant="contained" onClick={()=>{dispatch(fetchAllUsers());}}>Fetch All Users</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(fetchUsersById({id:userId}));}}>Fetch User With ID (userId)</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(fetchUsersById({id:'userId'}));}}>Fetch User With ID Error</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(deleteUsers({id:userId}));}}>Delete User With ID (userId)</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(deleteUsers({id:'userId'}));}}>Delete User With ID Error</Button>
            <br></br>
            <Button variant="contained" onClick={onClickCreateUser}>Create New User Success / Error</Button>
            <br></br>
            <TextField inputProps={{'data-testid':"tf"}} fullWidth multiline value={value}/>
        </div>
    );
}

export default UsersPage;