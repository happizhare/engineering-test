import UsersPage from ".";
import { render, screen, waitFor, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../../store';

it('Test Click Get All Users', async () => {
    global.fetch = jest.fn().mockResolvedValue({ 
        status: 200,
        json: () => (
            {
                data: [
                    {
                        "id": "53451baa-7ec1-4b32-8bc4-89566d471927",
                        "name": "Tyshawn Hayes",
                        "email": "Amiya.Kutch37@gmail.com"
                    },
                    {
                        "id": "7d99e201-f8be-47cf-ae6e-798325ecfac2",
                        "name": "Margaretta Nolan",
                        "email": "Vidal.Streich@hotmail.com"
                    },
                    {
                        "id": "ef969255-5afa-4fb3-9ce8-c7c5bd176635",
                        "name": "Roger Tremblay",
                        "email": "Candace_Spinka@hotmail.com"
                    }
                ]
            }
        )
    });

    const {getByTestId, asFragment} = render(<Provider store={store}><UsersPage /></Provider>);

    await act(() => {
        const loadButton = getByTestId('btn-fetch-all-users');

        userEvent.click(loadButton);
    });

    const el = await screen.getByTestId('tf')
    expect(el).toHaveTextContent(/53451baa-7ec1-4b32-8bc4-89566d471927/i)
});