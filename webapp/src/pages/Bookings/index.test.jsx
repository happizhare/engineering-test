import BookingsPage from ".";
import { render, screen, act } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { store } from '../../store';

it('Test Click Get All Bookings', async () => {
    global.fetch = jest.fn().mockResolvedValue({ 
        status: 200,
        json: () => (
            {
                "data": [
                    {
                        "id": "e39ce8ee-15ca-471a-9190-5ff916c9afbb",
                        "user": "ParcName1696382219861",
                        "parc": "ParcDescription1696382219861",
                        "bookingdate": "ParcDescription1696382219861",
                        "comments": "Comments-1696382219861"
                    },
                    {
                        "id": "371247a4-6393-42d7-ab95-0dd96f380682",
                        "user": "ParcName1696382220036",
                        "parc": "ParcDescription1696382220036",
                        "bookingdate": "ParcDescription1696382220036",
                        "comments": "Comments-1696382220036"
                    },
                ]
            }
        )
    });

    const {getByTestId, asFragment} = await render(<Provider store={store}><BookingsPage /></Provider>);

    await act(() => {
        const button = getByTestId('btn-fetch-all-bookings');
        userEvent.click(button);
    });

    const el = await screen.getByTestId('tf')
    expect(el).toHaveTextContent(/e39ce8ee-15ca-471a-9190-5ff916c9afbb/i)
});