import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { Booking } from '../../reducers/bookings';
import { fetchAllBookings, fetchBookingById, deleteBooking, createBooking } from '../../api';
import { AppDispatch } from '../../store';

import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

function BookingsPage() {
    const dispatch = useDispatch<AppDispatch>();
    const bookings:Array<Booking> = useSelector( (state:RootState) => state.bookings);

    let value = '';

    let id:string = bookings.length > 0 ? bookings[0].id : '';

    bookings.forEach((u:Booking)=>{
        if (value !== '') {
            value += "\n";
        }
        value += `${u.id},${u.user},${u.parc},${u.bookingdate},${u.comments}`;
    });

    function onClickCreateBookings() {
        const t = Date.now();
        let user:string = `ParcName${t}`;
        let parc:string = `ParcDescription${t}`;
        let bookingdate:string = `ParcDescription${t}`;
        let comments:string = `Comments-${t}`;
        dispatch(createBooking({user, parc, bookingdate, comments}));
    }

    function onClickCreateBookingsErr() {
        const t = Date.now();
        let user:string = `Error${t}`;
        let parc:string = '';
        let bookingdate:string = '';
        let comments:string = '';
        dispatch(createBooking({user, parc, bookingdate, comments}));
    }
    
    return (
        <div>
            <Button data-testid="btn-fetch-all-bookings" variant="contained" onClick={()=>{dispatch(fetchAllBookings());}}>Fetch All Bookings</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(fetchBookingById({id}));}}>Fetch Booking With ID (Id)</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(fetchBookingById({id:'booking'}));}}>Fetch Booking With ID Error</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(deleteBooking({id}));}}>Delete Booking With ID (Id)</Button>
            <br></br>
            <Button variant="contained" onClick={()=>{dispatch(deleteBooking({id:'booking'}));}}>Delete Booking With ID Error</Button>
            <br></br>
            <Button variant="contained" onClick={onClickCreateBookings}>Create New Booking Success / Error</Button>
            <br></br>
            <Button variant="contained" onClick={onClickCreateBookingsErr}>Create New Booking Error</Button>
            <br></br>
            <TextField inputProps={{'data-testid':"tf"}} fullWidth multiline value={value}/>
        </div>
    );
}

export default BookingsPage;