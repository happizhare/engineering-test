import { configureStore } from '@reduxjs/toolkit';
import users from '../reducers/users';
import parcs from '../reducers/parcs';
import bookings from '../reducers/bookings';
import loading from '../reducers/loading';
import message from '../reducers/message';

export const store = configureStore({
    reducer: {
        users,
        parcs,
        bookings,
        loading,
        message
    }
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;