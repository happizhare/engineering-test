import React from 'react';
import { RootState } from '../../store';
import { useSelector } from 'react-redux';
import Typography from '@mui/material/Typography';

function MessageBox() {
    const message:string = useSelector( (state:RootState) => state.message);
    
    return (
      <Typography id="modal-modal-title" variant="h6" component="h2">
        {message}
      </Typography>
    );
}

export default MessageBox;