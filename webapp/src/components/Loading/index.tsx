import React from 'react';
import { RootState } from '../../store';
import { useSelector } from 'react-redux';

import CircularProgress from '@mui/material/CircularProgress';

function Loading() {
    const isLoading:boolean = useSelector( (state:RootState) => state.loading);

    return (
        isLoading ? <CircularProgress/> : <></>
    );
}

export default Loading;