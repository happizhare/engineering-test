import { createAsyncThunk } from '@reduxjs/toolkit';

/*function sleep(m:number) {
    return new Promise((resolve) => {
        setTimeout(
            resolve, 5000
        )
    });
}*/

export const fetchAllUsers = createAsyncThunk("fetchAllUsers", async (_, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/users`);
        if (res.status === 200) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const fetchUsersById = createAsyncThunk("fetchUsersById", async ({id}:{id:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/users/${id}`);
        if (res.status === 200) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const deleteUsers = createAsyncThunk("deleteUsers", async ({id}:{id:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/users/${id}`, {
            method:'DELETE',
        });
        if (res.status === 200) {
            return {id};
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const createUser = createAsyncThunk("createUser", async ({name, email}:{name:string, email:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/users/`, {
            method:'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name, email
            })
        });
        if (res.status === 201) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const fetchAllParcs = createAsyncThunk("fetchAllParcs", async (_, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/parcs`);
        if (res.status === 200) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const fetchParcsById = createAsyncThunk("fetchParcsById", async ({id}:{id:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/parcs/${id}`);
        if (res.status === 200) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const deleteParcs = createAsyncThunk("deleteParcs", async ({id}:{id:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/parcs/${id}`, {
            method:'DELETE',
        });
        if (res.status === 200) {
            return {id};
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const createParcs = createAsyncThunk("createParcs", async ({name, description}:{name:string, description:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/parcs/`, {
            method:'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name, description
            })
        });
        if (res.status === 201) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const fetchAllBookings = createAsyncThunk("fetchAllBookings", async (_, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/bookings`);
        if (res.status === 200) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const fetchBookingById = createAsyncThunk("fetchBookingById", async ({id}:{id:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/bookings/${id}`);
        if (res.status === 200) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const deleteBooking = createAsyncThunk("deleteBooking", async ({id}:{id:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/bookings/${id}`, {
            method:'DELETE',
        });
        if (res.status === 200) {
            return {id};
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});

export const createBooking= createAsyncThunk("createBooking", async ({user, parc, bookingdate, comments}:{user:string, parc:string, bookingdate:string, comments:string}, {rejectWithValue}) => {
    try {
        const res = await fetch(`/api/1/bookings/`, {
            method:'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                user, parc, bookingdate, comments
            })
        });
        if (res.status === 201) {
            return res?.json();
        }
        return rejectWithValue(res.status);
    } catch (err) {
        return rejectWithValue(500);
    }
});