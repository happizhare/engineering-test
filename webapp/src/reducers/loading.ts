import { createSlice } from '@reduxjs/toolkit';
import { fetchAllUsers, fetchUsersById, deleteUsers, createUser, fetchAllParcs, fetchParcsById, deleteParcs, createParcs, fetchAllBookings, fetchBookingById, deleteBooking, createBooking } from '../api';

const initialState: boolean = false;

export const loadingSlice = createSlice({
    name: 'loading',
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        // fetchAllUsers API
        builder.addCase(fetchAllUsers.pending, (state, action) => {
            return true;
        });
        builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(fetchAllUsers.rejected, (state, action) => {
            return false;
        });
        // fetchUsersById API
        builder.addCase(fetchUsersById.pending, (state, action) => {
            return true;
        });
        builder.addCase(fetchUsersById.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(fetchUsersById.rejected, (state, action) => {
           return false;
        });
        // deleteUsers API
        builder.addCase(deleteUsers.pending, (state, action) => {
            return true;
        });
        builder.addCase(deleteUsers.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(deleteUsers.rejected, (state, action) => {
            return false;
        });
        // createUser API
        builder.addCase(createUser.pending, (state, action) => {
            return true;
        });
        builder.addCase(createUser.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(createUser.rejected, (state, action) => {
            return false;
        });

        // fetchAllParcs API
        builder.addCase(fetchAllParcs.pending, (state, action) => {
            return true;
        });
        builder.addCase(fetchAllParcs.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(fetchAllParcs.rejected, (state, action) => {
            return false;
        });
        // fetchParcsById API
        builder.addCase(fetchParcsById.pending, (state, action) => {
            return true;
        });
        builder.addCase(fetchParcsById.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(fetchParcsById.rejected, (state, action) => {
            return false;
        });
        // deleteParcs API
        builder.addCase(deleteParcs.pending, (state, action) => {
            return true;
        });
        builder.addCase(deleteParcs.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(deleteParcs.rejected, (state, action) => {
            return false;
        });
        // createParcs API
        builder.addCase(createParcs.pending, (state, action) => {
            return true;
        });
        builder.addCase(createParcs.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(createParcs.rejected, (state, action) => {
            return false;
        });

        // fetchAllBookings API
        builder.addCase(fetchAllBookings.pending, (state, action) => {
            return true;
        });
        builder.addCase(fetchAllBookings.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(fetchAllBookings.rejected, (state, action) => {
            return false;
        });
        // fetchBookingsById API
        builder.addCase(fetchBookingById.pending, (state, action) => {
            return true;
        });
        builder.addCase(fetchBookingById.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(fetchBookingById.rejected, (state, action) => {
            return false;
        });
        // deleteBookings API
        builder.addCase(deleteBooking.pending, (state, action) => {
            return true;
        });
        builder.addCase(deleteBooking.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(deleteBooking.rejected, (state, action) => {
            return false;
        });
        // createBookings API
        builder.addCase(createBooking.pending, (state, action) => {
            return true;
        });
        builder.addCase(createBooking.fulfilled, (state, action) => {
            return false;
        });
        builder.addCase(createBooking.rejected, (state, action) => {
            return false;
        });
    }
});

export const actions = loadingSlice.actions;

export default loadingSlice.reducer;