import { createSlice } from '@reduxjs/toolkit';
import { fetchAllParcs, fetchParcsById, deleteParcs, createParcs } from '../api';

export class Parc {
    id: string;
    name: string;
    description: string;

    constructor(id:string, name:string, description:string) {
        this.id = id;
        this.name = name;
        this.description = description;
    }
}

const initialState: Array<Parc> = new Array<Parc>();

export const parcsSlice = createSlice({
    name: 'parcs',
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        // fetchAllParcs API
        builder.addCase(fetchAllParcs.fulfilled, (state, action) => {
            return action.payload.data;
        });
        // fetchParcsById API
        builder.addCase(fetchParcsById.fulfilled, (state, action) => {
            // check user is existed
            const newState:Array<Parc> = state.slice();
            const index = newState.findIndex(item => item.id === action.payload.id);
            if (index >= 0) {
                newState[index] = action.payload;
                return newState;
            }
            return state;
        });
        // deleteParcs API
        builder.addCase(deleteParcs.fulfilled, (state, action) => {
            const newState:Array<Parc> = new Array<Parc>();
            state.forEach( (item:Parc) => {
                if (item.id !== action.payload.id) {
                    newState.push(item);
                }
            });
            return newState;
        });
        // createParcs API
        builder.addCase(createParcs.fulfilled, (state, action) => {
            const newState:Array<Parc> = state.slice();
            newState.push(action.payload);
            return newState;
        });
    }
});

export default parcsSlice.reducer;