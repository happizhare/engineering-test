import { createSlice } from '@reduxjs/toolkit';
import { fetchAllUsers, fetchUsersById, deleteUsers, createUser } from '../api';

export class User {
    id: string;
    name: string;
    email: string;

    constructor(id:string, name:string, email:string) {
        this.id = id;
        this.name = name;
        this.email = email;
    }
}

const initialState: Array<User> = new Array<User>();

const usersSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        // omit existing reducers here
    },
    extraReducers: (builder) => {
        // fetchAllUsers API
        builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
            return action.payload.data;
        });
        // fetchUsersById API
        builder.addCase(fetchUsersById.fulfilled, (state, action) => {
            // check user is existed
            const newState:Array<User> = state.slice();
            const index = newState.findIndex(item => item.id === action.payload.id);
            if (index >= 0) {
                newState[index] = action.payload;
                return newState;
            }
            return state;
        });
        // deleteUsers API
        builder.addCase(deleteUsers.fulfilled, (state, action) => {
            const newState:Array<User> = new Array<User>();
            state.forEach( (item:User) => {
                if (item.id !== action.payload.id) {
                    newState.push(item);
                }
            });
            return newState;
        });
        // createUser API
        builder.addCase(createUser.fulfilled, (state, action) => {
            const newState:Array<User> = state.slice();
            newState.push(action.payload);
            return newState;
        });
    }
});

export default usersSlice.reducer;