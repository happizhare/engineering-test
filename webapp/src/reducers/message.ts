import { createSlice } from '@reduxjs/toolkit';
import { fetchAllUsers, fetchUsersById, deleteUsers, createUser, fetchAllParcs, fetchParcsById, deleteParcs, createParcs, fetchAllBookings, fetchBookingById, deleteBooking, createBooking } from '../api';

const initialState: string = '';

export const messageSlice = createSlice({
    name: 'message',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        // fetchAllUsers API
        builder.addCase(fetchAllUsers.pending, (state, action) => {
            return 'Calling Fetching All Users API Started';
        });
        builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
            return 'Calling Fetching All Users API Success';
        });
        builder.addCase(fetchAllUsers.rejected, (state, action) => {
            return 'Calling Fetching All Users API Error';
        });
        // fetchUsersById API
        builder.addCase(fetchUsersById.pending, (state, action) => {
            return 'Calling Fetch Users By ID API Started';
        });
        builder.addCase(fetchUsersById.fulfilled, (state, action) => {
            return 'Calling Fetch Users By ID API Success';
        });
        builder.addCase(fetchUsersById.rejected, (state, action) => {
            return 'Calling Fetch Users By ID API Error';
        });
        // deleteUsers API
        builder.addCase(deleteUsers.pending, (state, action) => {
            return 'Calling Delete User API Started';
        });
        builder.addCase(deleteUsers.fulfilled, (state, action) => {
            return 'Calling Delete User API Success';
        });
        builder.addCase(deleteUsers.rejected, (state, action) => {
            return 'Calling Delete User API Error';
        });
        // createUser API
        builder.addCase(createUser.pending, (state, action) => {
            return 'Calling Create User API Started';
        });
        builder.addCase(createUser.fulfilled, (state, action) => {
            return 'Calling Create User API Success';
        });
        builder.addCase(createUser.rejected, (state, action) => {
            return 'Calling Create User API Error';
        });

        // fetchAllParcs API
        builder.addCase(fetchAllParcs.pending, (state, action) => {
            return 'Calling Fetching All Parcs API Started';
        });
        builder.addCase(fetchAllParcs.fulfilled, (state, action) => {
            return 'Calling Fetching All Parcs API Success';
        });
        builder.addCase(fetchAllParcs.rejected, (state, action) => {
            return 'Calling Fetching All Parcs API Error';
        });
        // fetchParcsById API
        builder.addCase(fetchParcsById.pending, (state, action) => {
            return 'Calling Fetch Parcs By ID API Started';
        });
        builder.addCase(fetchParcsById.fulfilled, (state, action) => {
            return 'Calling Fetch Parcs By ID API Success';
        });
        builder.addCase(fetchParcsById.rejected, (state, action) => {
            return 'Calling Fetch Parcs By ID API Error';
        });
        // deleteParcs API
        builder.addCase(deleteParcs.pending, (state, action) => {
            return 'Calling Delete Parcs API Started';
        });
        builder.addCase(deleteParcs.fulfilled, (state, action) => {
            return 'Calling Delete Parcs API Success';
        });
        builder.addCase(deleteParcs.rejected, (state, action) => {
            return 'Calling Delete Parcs API Error';
        });
        // createParcs API
        builder.addCase(createParcs.pending, (state, action) => {
            return 'Calling Create Parcs API Started';
        });
        builder.addCase(createParcs.fulfilled, (state, action) => {
            return 'Calling Create Parcs API Success';
        });
        builder.addCase(createParcs.rejected, (state, action) => {
            return 'Calling Create Parcs API Error';
        });

        // fetchAllBookings API
        builder.addCase(fetchAllBookings.pending, (state, action) => {
            return 'Calling Fetching All Bookings API Started';
        });
        builder.addCase(fetchAllBookings.fulfilled, (state, action) => {
            return 'Calling Fetching All Bookings API Success';
        });
        builder.addCase(fetchAllBookings.rejected, (state, action) => {
            return 'Calling Fetching All Bookings API Error';
        });
        // fetchBookingsById API
        builder.addCase(fetchBookingById.pending, (state, action) => {
            return 'Calling Fetch Bookings By ID API Started';
        });
        builder.addCase(fetchBookingById.fulfilled, (state, action) => {
            return 'Calling Fetch Bookings By ID API Success';
        });
        builder.addCase(fetchBookingById.rejected, (state, action) => {
            return 'Calling Fetch Bookings By ID API Error';
        });
        // deleteBookings API
        builder.addCase(deleteBooking.pending, (state, action) => {
            return 'Calling Delete Bookings API Started';
        });
        builder.addCase(deleteBooking.fulfilled, (state, action) => {
            return 'Calling Delete Bookings API Success';
        });
        builder.addCase(deleteBooking.rejected, (state, action) => {
            return 'Calling Delete Bookings API Error';
        });
        // createBookings API
        builder.addCase(createBooking.pending, (state, action) => {
            return 'Calling Create Bookings API Started';
        });
        builder.addCase(createBooking.fulfilled, (state, action) => {
            return 'Calling Create Bookings API Success';
        });
        builder.addCase(createBooking.rejected, (state, action) => {
            return 'Calling Create Bookings API Error';
        });
    }
});

export const actions = messageSlice.actions;

export default messageSlice.reducer;