import { waitFor, act } from '@testing-library/react';
import { store } from '../store';
import { fetchAllBookings, fetchBookingById, deleteBooking, createBooking } from '../api';
import { configureStore } from '@reduxjs/toolkit';
import bookings from './bookings';
import message from './message';

it('Test Click Get All Bookings', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 200,
        json: () => (
            {
                "data": [
                    {
                        "id": "e39ce8ee-15ca-471a-9190-5ff916c9afbb",
                        "user": "ParcName1696382219861",
                        "parc": "ParcDescription1696382219861",
                        "bookingdate": "ParcDescription1696382219861",
                        "comments": "Comments-1696382219861"
                    },
                    {
                        "id": "371247a4-6393-42d7-ab95-0dd96f380682",
                        "user": "ParcName1696382220036",
                        "parc": "ParcDescription1696382220036",
                        "bookingdate": "ParcDescription1696382220036",
                        "comments": "Comments-1696382220036"
                    },
                ]
            }
        )
    });

    await act(() => {
        store.dispatch(fetchAllBookings());
    });

    await waitFor(() => {
        expect(store.getState().bookings.find(item => item.bookingdate === 'ParcDescription1696382219861')).not.toBe(null);
    })
});

it('Test Click Get Bookings By ID', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 200,
        json: () => (
            {
                "id": "e39ce8ee-15ca-471a-9190-5ff916c9afbb",
                "user": "ParcName1696382219861",
                "parc": "ParcDescription1696382219861",
                "bookingdate": "ParcDescription1696382219861",
                "comments": "Comments-1696382219861"
            }
        )
    });

    await act(() => {
        const id:string = 'e39ce8ee-15ca-471a-9190-5ff916c9afbb';
        store.dispatch(fetchBookingById({id}));
    });

    await waitFor(() => {
        expect(store.getState().bookings.find(item => item.id === 'e39ce8ee-15ca-471a-9190-5ff916c9afbb')).not.toBeUndefined();
    })
});

it('Test Click Get Bookings By ID Error (400)', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 400
    });

    const _store = configureStore({
        reducer: {
            bookings,
            message
        }
    });

    await act(() => {
        const id:string = 'e39ce8ee-15ca-471a-9190-5ff916c9afbb';
        _store.dispatch(fetchBookingById({id}));
    });

    await waitFor(() => {
        expect(_store.getState().message).toBe('Calling Fetch Bookings By ID API Error');
    })
});

it('Test Click Get Bookings By ID Error (404)', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 404
    });

    const _store = configureStore({
        reducer: {
            bookings,
            message
        }
    });

    await act(() => {
        const id:string = 'e39ce8ee-15ca-471a-9190-5ff916c9afbb';
        _store.dispatch(fetchBookingById({id}));
    });

    await waitFor(() => {
        expect(_store.getState().message).toBe('Calling Fetch Bookings By ID API Error');
    })
});

it('Test Delete Bookings', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 200
    });
    
    const _store = configureStore({
        reducer: {
            bookings,
        },
        preloadedState: {
            bookings: [
                {
                    "id": "e39ce8ee-15ca-471a-9190-5ff916c9afbb",
                    "user": "ParcName1696382219861",
                    "parc": "ParcDescription1696382219861",
                    "bookingdate": "ParcDescription1696382219861",
                    "comments": "Comments-1696382219861"
                },
                {
                    "id": "371247a4-6393-42d7-ab95-0dd96f380682",
                    "user": "ParcName1696382220036",
                    "parc": "ParcDescription1696382220036",
                    "bookingdate": "ParcDescription1696382220036",
                    "comments": "Comments-1696382220036"
                },
            ]
        }
    });

    await act(() => {
        const id:string = 'e39ce8ee-15ca-471a-9190-5ff916c9afbb';
        _store.dispatch(deleteBooking({id}));
    });

    await waitFor(() => {
        expect(_store.getState().bookings.find(item => item.id === 'e39ce8ee-15ca-471a-9190-5ff916c9afbb')).toBeUndefined();
    })
});

it('Test Delete Bookings Error - 404', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 404
    });
    
    const _store = configureStore({
        reducer: {
            bookings,
            message
        }
    });

    await act(() => {
        const id:string = 'e39ce8ee-15ca-471a-9190-5ff916c9afbb';
        _store.dispatch(deleteBooking({id}));
    });

    await waitFor(() => {
        expect(_store.getState().message).toBe('Calling Delete Bookings API Error');
    })
});

it('Test Create Bookings', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 201,
        json: () => (
            {
                "id": "e39ce8ee-15ca-471a-9190-5ff916c9afbb",
                "user": "userA",
                "parc": "parc",
                "bookingdate": "bookingdate",
                "comments": "comments"
            }
        )
    });
    
    const _store = configureStore({
        reducer: {
            bookings,
        }
    });

    await act(() => {
        const user:string = 'userA';
        const parc:string = 'parc';
        const bookingdate:string = 'bookingdate';
        const comments:string = 'comments';
        _store.dispatch(createBooking({user, parc, bookingdate, comments}));
    });

    await waitFor(() => {
        expect(_store.getState().bookings.find(item => item.user === 'userA')).not.toBeUndefined();
    })
});

it('Test Create Bookings Error 404', async () => {
    global.fetch = jest.fn().mockResolvedValue({
        status: 404
    });
    
    const _store = configureStore({
        reducer: {
            bookings,
            message
        }
    });

    await act(() => {
        const user:string = 'userA';
        const parc:string = 'parc';
        const bookingdate:string = 'bookingdate';
        const comments:string = 'comments';
        _store.dispatch(createBooking({user, parc, bookingdate, comments}));
    });

    await waitFor(() => {
        expect(_store.getState().message).toBe('Calling Create Bookings API Error');
    })
});