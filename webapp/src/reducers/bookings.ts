import { createSlice } from '@reduxjs/toolkit';
import { fetchAllBookings, fetchBookingById, deleteBooking, createBooking } from '../api';

export class Booking {
    id: string;
    user: string;
    parc: string;
    bookingdate: string;
    comments: string;

    constructor(id:string, user:string, parc:string, bookingdate:string, comments:string) {
        this.id = id;
        this.user = user;
        this.parc = parc;
        this.bookingdate = bookingdate;
        this.comments = comments;
    }
}

const initialState: Array<Booking> = new Array<Booking>();

export const bookingsSlice = createSlice({
    name: 'bookings',
    initialState,
    reducers: {
    },
    extraReducers: (builder) => {
        // fetchAllBookings API
        builder.addCase(fetchAllBookings.fulfilled, (state, action) => {
            return action.payload.data;
        });
        // fetchBookingById API
        builder.addCase(fetchBookingById.fulfilled, (state, action) => {
            // check user is existed
            const newState:Array<Booking> = state.slice();
            const index = newState.findIndex(item => item.id === action.payload.id);
            if (index >= 0) {
                newState[index] = action.payload;
                return newState;
            }
            return state;
        });
        // deleteBooking API
        builder.addCase(deleteBooking.fulfilled, (state, action) => {
            const newState:Array<Booking> = new Array<Booking>();
            state.forEach( (item:Booking) => {
                if (item.id !== action.payload.id) {
                    newState.push(item);
                }
            });
            return newState;
        });
        // createBooking API
        builder.addCase(createBooking.fulfilled, (state, action) => {
            const newState:Array<Booking> = state.slice();
            newState.push(action.payload);
            return newState;
        });
    }
});

export default bookingsSlice.reducer;