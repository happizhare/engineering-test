write any notes here

Task 1:
Please read the share.txt in folder/taskOne

Task 2:
Please read the share.txt in folder/taskTwo

Task 3:
- Run `docker-compose up -d --force-recreate`
- Run `docker exec -it engineering-testgit_eurocamp-api_1 npm run seed:run`

- Go to webapp folder
- Run `yarn` to install the dependencies

Open browser 'http://localhost:3000/'
- You can see the ugly layout, 
- Having Tabs "Users" / "PARCS" / "BOOKINGS", You can switching the tabs and click different buttons for calling the API

At the top, there is message telling the status of the API


If you have any problems or question, please feel free to contact me. Thanks.